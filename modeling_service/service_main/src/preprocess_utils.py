from PIL import Image
import os
import numpy as np

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def preprocess_image(image: np.ndarray, input_shape: tuple) -> np.ndarray:
    image = image.convert("RGB")
    image = image.resize((224, 224))

    image = np.array(image) / 255.0

    image = np.array(image, dtype=np.float32)
    image = np.expand_dims(image, axis=0)

    return image


def load_model(config, path):
    if config["services"]["classifier"]["onnx"]:
        import onnxruntime as ort

        providers = ["CPUExecutionProvider"]
        model = ort.InferenceSession(path, providers=providers)

        return model
    else:
        if config["services"]["classifier"]["edge"]:
            from tflite_runtime.interpreter import Interpreter

            interpreter = Interpreter(model_path=path)
            interpreter.allocate_tensors()
        else:
            import tensorflow as tf

            interpreter = tf.lite.Interpreter(model_path=path)
            interpreter.allocate_tensors()

        return interpreter
