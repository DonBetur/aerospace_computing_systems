# %%
from config.mission_parameters import (
    FINAL_FUEL_MASS,
    MACHINE_ZERO,
    OBC_ID,
    SIMULATOR_ID,
)
from libs.ballistic import sat_is_in_thrusting_allowed_zone, sun_visibility
from libs.can_utils import bus, send_args_to_can_bus, unpack_args_from_str


# %%
def calc_new_sat_state(t, y, cur_sat_state, strategy):
    m_sum = y[1]
    r_vector = y[2:5]

    sun_vis = sun_visibility(t, r_vector)

    if cur_sat_state == "initial":
        if sun_vis > 0.5:
            new_sat_state = "heat_accumulation"
        else:
            new_sat_state = "eclipse"

    elif cur_sat_state == "heat_accumulation":
        if abs(sun_vis - 0.5) < MACHINE_ZERO:
            new_sat_state = "eclipse"
        else:
            if sat_is_in_thrusting_allowed_zone(y, strategy):
                new_sat_state = "orbital_orientation"
            else:
                new_sat_state = "waiting_thrusting_allowed_zone"

    elif cur_sat_state in (
        "orbital_orientation",
        "waiting_thrusting_allowed_zone",
    ):
        new_sat_state = "thrusting"

    elif cur_sat_state == "eclipse":
        new_sat_state = "heat_accumulation"

    elif cur_sat_state == "thrusting":
        if m_sum > FINAL_FUEL_MASS:
            new_sat_state = "eclipse" if sun_vis < 0.5 else "heat_accumulation"
        else:
            new_sat_state = "out_of_fuel"

    elif cur_sat_state == "out_of_fuel":
        new_sat_state = cur_sat_state  # to avoid error at simulation end

    else:
        raise Exception("Unknown satellite state")

    return new_sat_state


if __name__ == "__main__":
    while True:
        msg = bus.recv()
        if msg.arbitration_id == SIMULATOR_ID:
            data = msg.data.decode("ascii")
            if data[0] == "[":
                whole_msg = data
                while data[-1] != "]":
                    msg = bus.recv()
                    data = msg.data.decode("ascii")
                    whole_msg += data

            t, y, cur_sat_state, strategy = unpack_args_from_str(whole_msg)
            new_sat_state = calc_new_sat_state(t, y, cur_sat_state, strategy)
            send_args_to_can_bus(new_sat_state, arbitration_id=OBC_ID)
