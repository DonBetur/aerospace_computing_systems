import ctypes
import platform
from pathlib import Path

# From Python 3.8 onwards, there is a reported bug in CDLL.__init__()
mode = dict(winmode=0) if platform.python_version() >= "3.8" else dict()

cant_folder = Path(__file__).parent
lib = ctypes.CDLL(cant_folder.joinpath("capy.so"), **mode)


class cant(object):
    def __init__(self):
        # Declare input and output types for each method you intend to use
        lib.init.argtypes = []
        lib.init.restype = ctypes.c_void_p

        lib.InitId.argtypes = []
        lib.InitId.restype = None

        lib.ClearIdStructure.argtypes = []
        lib.ClearIdStructure.restype = None

        lib.AddIdStruct.argtypes = []
        lib.AddIdStruct.restype = None

        lib.getIdAttrSize.argtypes = []
        lib.getIdAttrSize.restype = ctypes.c_int

        lib.getLoadId.argtypes = []
        lib.getLoadId.restype = ctypes.c_bool

        lib.codeId.argtypes = []
        lib.codeId.restype = None

        lib.decodeId.argtypes = []
        lib.decodeId.restype = None

        lib.setAttrByNdx.argtypes = [ctypes.c_int, ctypes.c_uint8]
        lib.setAttrByNdx.restype = ctypes.c_bool

        lib.setAttrByName.argtypes = []
        lib.setAttrByName.restype = ctypes.c_bool

        lib.getAttrByNdx.argtypes = [ctypes.c_int]
        lib.getAttrByNdx.restype = ctypes.c_uint8

        lib.getAttrByName.argtypes = []
        lib.getAttrByName.restype = ctypes.c_uint8

        lib.getAdr.argtypes = []
        lib.getAdr.restype = ctypes.c_uint32

        lib.test.argtypes = []
        lib.test.restype = None

        self.obj = lib.init()

    def InitId(self):
        lib.InitId(self.obj)

    def ClearIdStructure(self):
        lib.ClearIdStructure(self.obj)

    def AddIdStruct(self, Name, Length, Key):
        lib.AddIdStruct(self.obj, Name, Length, Key)

    def getIdAttrSize(self):
        return lib.getIdAttrSize(self.obj)

    def getLoadId(self):
        return lib.getLoadId(self.obj)

    def codeId(self):
        lib.codeId(self.obj)

    def decodeId(self):
        lib.decodeId(self.obj)

    def setAttrByNdx(self, ndx, Val):
        return lib.setAttrByNdx(self.obj, ndx, Val)

    def setAttrByName(self, Name, Val):
        return lib.setAttrByName(self.obj, Name, Val)

    def getAttrByNdx(self, ndx):
        return lib.getAttrByNdx(self.obj, ndx)

    def getAttrByName(self, Name):
        return lib.getAttrByName(self.obj, Name)

    def getAdr(self):
        return lib.getAdr(self.obj)

    def test(self, s):
        lib.test(self.obj, s)

    def SetId(self, NM, LN, KY):
        # print(len(NM))
        if len(NM) == len(LN) == len(KY):
            self.ClearIdStructure()
            # print("getIdAttrSize=", self.getIdAttrSize())

            for i in range(len(NM)):
                print(NM[i], " ", LN[i], " ", KY[i])
                self.AddIdStruct(bytes(NM[i], "ascii"), int(LN[i]), int(KY[i]))
            # print("InitID")

            self.InitId()
            # print("getIdAttrSize=", self.getIdAttrSize())
        else:
            print("CANT ERROR!!! len(NM)!=len(LN)")
