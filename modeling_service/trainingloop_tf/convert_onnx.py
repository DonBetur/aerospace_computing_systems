import tf2onnx
from tf2onnx import optimizer
import tensorflow as tf

tf_model = tf.keras.models.load_model("weights/bestmodel.h5", compile=False)

onnx_model, _ = tf2onnx.convert.from_keras(tf_model)

with open("weights/onnx_model.onnx", "wb") as f:
    f.write(onnx_model.SerializeToString())
