import typing as tp
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

import numpy as np

# import onnxruntime as ort
from src.preprocess_utils import load_model, preprocess_image


class PlanetClassifier:
    def __init__(self, config: tp.Dict):
        self.config = config
        if config["services"]["classifier"]["onnx"]:
            self._model_path = config["services"]["classifier"]["model_path_onnx"]
        else:
            self._model_path = config["services"]["classifier"]["model_path_tflite"]

        self.threshold = config["services"]["classifier"]["threshold"]
        self.classes = config["services"]["classifier"]["classes"]

        self._model = load_model(self.config, self._model_path)

    def predict_proba(self, image: np.ndarray) -> tp.Dict[str, float]:
        return self._postprocess_predict_proba(self._predict(image))

    def _predict(self, image: np.ndarray) -> np.ndarray:
        if self.config["services"]["classifier"]["onnx"]:
            input_name = self._model.get_inputs()[0].name
            output_name = self._model.get_outputs()[0].name

            input_shape = self._model.get_inputs()[0].shape

            batch = preprocess_image(image, input_shape)

            output_data = self._model.run([output_name], {input_name: batch})[0]

            return output_data
        else:
            input_details = self._model.get_input_details()
            output_details = self._model.get_output_details()

            input_shape = input_details[0]["shape"]

            batch = preprocess_image(image, input_shape)

            self._model.set_tensor(input_details[0]["index"], batch)

            self._model.invoke()

            output_data = self._model.get_tensor(output_details[0]["index"])

            return output_data

    def _postprocess_predict_proba(self, predict: np.ndarray) -> tp.Dict[str, float]:
        list_ind = np.where(predict[0] >= self.threshold)
        list_ind = " ".join(map(str, list_ind[0]))

        return list_ind
