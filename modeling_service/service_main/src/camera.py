import os

import picamera
import requests


class Camera:
    def __init__(self, save_path, api_url):
        self.save_path = save_path
        self.api_url = api_url

    def capture_photo(self):
        with picamera.PiCamera() as camera:
            camera.resolution = (640, 480)
            camera.framerate = 24
            camera.start_preview()

            image_path = os.path.join("./data/", "image.jpg")
            camera.capture(image_path)
            self.send_photo(image_path)

    def send_photo(self, image_path):
        files = {"image": open(image_path, "rb")}
        response = requests.post(self.api_url, files=files)
        if response.status_code == 200:
            print("Image sent successfully")
        else:
            print("Error sending image:", response.content)
